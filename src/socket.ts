import * as io from "socket.io-client";

const socket = io.connect(`${document.location.hostname}:8081`, {transports: ["websocket"]});

export default socket;