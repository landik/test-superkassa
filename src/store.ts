import {createStore, Store} from 'redux';

type TStore = {
    state: boolean
}

type TStoreAction = {
    type: 'CHANGE',
    state: boolean
}

export const store: Store<TStore, TStoreAction> = createStore<TStore, TStoreAction, any, any>((state = {state: false}, action) => {
    switch (action.type) {
        case "CHANGE":
            return {...state, state: action.state}
    }
    return state;
});