import * as React from 'react'
import ReactDOM = require('react-dom')
import socket from "./socket";
import {store} from "./store";

ReactDOM.render(<App/>, document.getElementById('root'))

function App() {

    const [state, setState] = React.useState(false);

    React.useEffect(() => {
        socket.on("update", (data: { state: boolean }) => {
            store.dispatch({type: 'CHANGE',state: data.state})
        })
        return () => {
            socket.off("update")
        }
    }, [])

    React.useEffect(() => {
        return store.subscribe(() => {
            setState(store.getState().state)
        })
    }, [])

    const update = () => {
        socket.emit('update')
    }

    return <div className={'row h-100 justify-content-center'}>
        <div className="col-auto align-self-center">
            <button className={'btn'} onClick={update}>{state ? 'ON' : 'OFF'}</button>
        </div>
    </div>

}

