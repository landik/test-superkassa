import {Server, Socket} from "socket.io";

const server = new Server(8081);

let state = false;

server.on("connection", (socket: Socket) => {
    socket.emit('update', {state})
    socket.on('update', () => {
        state = !state;
        server.emit('update', {state})
    })
});

